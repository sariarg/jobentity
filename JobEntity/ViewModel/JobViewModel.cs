﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JobEntity.Models;

namespace JobEntity.ViewModel
{
    public class JobViewModel
    {
        public List<JobModel> listJob { get; set; }
        public JobModel job { get; set; }
    }
}
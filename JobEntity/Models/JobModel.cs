﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JobEntity.Models
{
    public class JobModel
    {
        public int Job { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
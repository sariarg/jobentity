﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JobEntity.Models;
using JobEntity.ViewModel;

namespace JobEntity.Controllers
{
    public class JobController : Controller
    {
        public static JobViewModel vm = new JobViewModel();
        public static List<JobModel> lista = new List<JobModel>();
        // GET: Job
        public ActionResult Index()
        {
            vm.listJob = lista;

            return View(vm);
        }


        // POST: Job/Create
        [HttpPost]
        public JsonResult Create(JobModel md)
        {
            string msj="";
            bool error = true;
            try
            {
                int count = 0;
                if (lista.Count() > 0)
                {
                    JobModel lst = new JobModel();
                    lst = lista.LastOrDefault();
                    count = lst.Job;
                    count = count + 1;
                }
                else
                {
                    count = 0;
                }
                lista.Add(new JobModel() { Job = count, JobTitle = md.JobTitle, JobDescription = md.JobDescription, CreatedAt = DateTime.Now, ExpiresAt = md.ExpiresAt });
                vm.listJob = lista;
                error = true;
            }
            catch
            {
                msj = "A ocurrido un error";
                error = false;
            }

            return Json(new
            {
                success = error,
                mensaje = msj
            });
        }



        // POST: Job/Edit/5
        [HttpPost]
        public ActionResult Edit(JobModel md)
        {
            string msj = "";
            bool error = true;
            try
            {   
                foreach (var mc in lista.Where(x => x.Job == md.Job))
                {
                    mc.JobTitle = md.JobTitle;
                    mc.JobDescription = md.JobDescription;
                    mc.ExpiresAt = md.ExpiresAt;
                }
                vm.listJob = lista;
                error = true;
            }
            catch
            {
                msj = "A ocurrido un error";
                error = false;
            }

            return Json(new
            {
                success = error,
                mensaje = msj
            });
        }


      
        public ActionResult Search(int job)
        {
            JobModel obj = new JobModel();
            string msj = "";
            bool error = true;
            string fecha = "";
            try
            {
                foreach (var mc in lista.Where(x => x.Job == job))
                {
                    obj.Job = job;
                    obj.JobTitle = mc.JobTitle;
                    obj.JobDescription = mc.JobDescription;
                    obj.ExpiresAt =DateTime.Parse( mc.ExpiresAt.ToString());
                    fecha = obj.ExpiresAt.ToString("yyyy-MM-dd");
                }
              
                error = true;
            }
            catch
            {
                msj = "A ocurrido un error";
                error = false;
            }

            return Json(new
            { 
                obj = obj,
                success = error,
                mensaje = msj,
                fecha = fecha
            });
        }




    
        [HttpPost]
        public JsonResult Delete(int job)
        {
            string msj = "";
            bool error = true;
            try
            {

                int index = lista.FindIndex(X => X.Job == job);
                lista.RemoveAt(index);
                vm.listJob = lista;
                error = true;
            }
            catch
            {
                error = false;
                msj = "A ocurrido un error";
            }

            return Json(new
            {
                success = error,
                mensaje = msj
            });
        }
    }
}
